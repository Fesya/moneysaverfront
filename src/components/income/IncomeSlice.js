import { createSlice } from '@reduxjs/toolkit'

const incomeSlice = createSlice({
  name: 'income',
  initialState: {
    totalIncome: parseFloat('79547.00').toFixed(2)
  },
  reducers: {
    setIncome: (state, action) => {
      state.totalIncome = action.payload.totalIncome
    }
  }
})

export const selectIncome = state => state.income.totalIncome
export const { setIncome } = incomeSlice.actions
export default incomeSlice.reducer
