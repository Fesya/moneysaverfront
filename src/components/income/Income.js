import React from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { selectIncome, setIncome } from './IncomeSlice'

export default function Income () {
  const dispatch = useDispatch()
  const income = useSelector(selectIncome)

  const handleChange = amount => {
    if (isNaN(amount)) { return }
    dispatch(setIncome({ totalIncome: amount }))
  }
  return (
      <div>
        Total Income:
          <input value={income} onChange={event => handleChange(event.currentTarget.value)}/>
      </div>
  )
}
