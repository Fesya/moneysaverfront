
import React from 'react'
import { useSelector } from 'react-redux'
import { selectSaving } from './saving/SavingSlice'
import { selectIncome } from './income/IncomeSlice'
import { selectTotalPlannedExpenses } from './expenses/PlannedExpensesSlice'

export default function Statistics () {
  const income = useSelector(selectIncome)
  const saving = useSelector(selectSaving)
  const expensesPerMonth = useSelector(selectTotalPlannedExpenses)

  const savingPerMonth = parseFloat(income * (saving / 100)).toFixed(2)
  const savingPerYear = parseFloat(savingPerMonth * 12).toFixed(2)

  const date = new Date()
  const currentYear = date.getFullYear()
  const currentMonth = date.getMonth() + 1
  const daysAmount = new Date(currentYear, currentMonth, 0).getDate()
  console.log(expensesPerMonth)
  console.log(savingPerMonth)
  const moneyPerDay = parseFloat((income - (+expensesPerMonth + +savingPerMonth)) / daysAmount).toFixed(2)

  return (
        <div className={'Statistic'}>
            <div>Total Income: {income}</div>
            <div>Expenses per month:{expensesPerMonth} </div>
            <div>Money Per Day: {moneyPerDay} </div>
            <div>Savings Per Month: {savingPerMonth}</div>
            <div>Savings Per Year: {savingPerYear}</div>
        </div>
  )
}
