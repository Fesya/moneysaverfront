import { createSlice } from '@reduxjs/toolkit'

const plannedExpensesSlice = createSlice({
  name: 'plannedExpenses',
  initialState: {
    plannedExpenses: {}
  },
  reducers: {
    addPlannedExpense: (state, action) => {
      const { id } = action.payload
      state.plannedExpenses[id] = action.payload
    },
    editPlannedExpence: (state, action) => {
      const { id } = action.payload
      state.plannedExpenses[id] = action.payload
    },
    deletePlannedExpences: (state, action) => {
      delete state.plannedExpenses[action.payload.id]
    }
  }
})

export const selectPlannedExpenses = (state) => state.plannedExpenses.plannedExpenses
export const selectTotalPlannedExpenses = (state) => Object.values(state.plannedExpenses.plannedExpenses).map(o => o.amount).reduce((a, b) => +a + +b, 0)

export const { addPlannedExpense, deletePlannedExpences, editPlannedExpence } = plannedExpensesSlice.actions
export default plannedExpensesSlice.reducer
