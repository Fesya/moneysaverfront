import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { v4 as uuidv4 } from 'uuid'
import { selectPlannedExpenses, addPlannedExpense as addAction, deletePlannedExpences as deleteAction, editPlannedExpence as editAction } from './PlannedExpensesSlice'
import Expense from './Expense'

export default function PlannedExpenses () {
  const dispatch = useDispatch()
  const [newExpense, setNewExpense] = useState('')
  const plannedExpenses = useSelector(selectPlannedExpenses)

  const addPlannedExpense = () => {
    if (isNaN(newExpense)) {
      setNewExpense('')
      return
    }
    dispatch(addAction({
      id: uuidv4(),
      amount: newExpense
    }))
  }

  return (
<div>
    <h3>Daily expense</h3>
  <input placeholder={'New Amount'} value={newExpense} onChange={event => setNewExpense(event.currentTarget.value)}/>
  <input type={'button'} value={'SAVE'} onClick={() => addPlannedExpense()}/>
    <div>
<ul>
     {
       Object.values(plannedExpenses).map(expense => (
         <li key={expense.id}>
           <Expense
               value={expense.amount}
               handleEdit={(newAmount) => dispatch(editAction({ id: expense.id, amount: newAmount }))}
               handleDelete={() => dispatch(deleteAction({ id: expense.id }))}
           />
         </li>
       ))
     }
</ul>
    </div>
</div>
  )
}
