import React, { useState } from 'react'
import PropTypes from 'prop-types'

export default function Expense ({ value, handleDelete, handleEdit }) {
  const [editable, setEditable] = useState(false)
  const [newAmount, setNewAmount] = useState(value)
  const save = function () {
    setEditable(false)
    handleEdit(newAmount)
  }
  return (
      <div>
          {editable
            ? (
                <div>
                    <input value={newAmount} onChange={event => setNewAmount(event.currentTarget.value) }/>
                    <input type={'button'} value={'save'} onClick={() => { save() } }/>
                </div>
              )
            : (
                  <div>
                      <input value={value} readOnly/>
                      <input type={'button'} value={'edit'} onClick={() => setEditable(true)}/>
                      <input type={'button'} value={'delete'} onClick={() => handleDelete()}/>
                  </div>
              )
          }

    </div>
  )
}
Expense.propTypes = {
  value: PropTypes.number,
  handleDelete: PropTypes.func,
  handleEdit: PropTypes.func
}
