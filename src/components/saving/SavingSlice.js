import { createSlice } from '@reduxjs/toolkit'

const savingSlice = createSlice({
  name: 'saving',
  initialState: {
    saving: 5
  },
  reducers: {
    setSaving: (state, action) => {
      state.saving = action.payload.saving
    }
  }
})

export const selectSaving = state => state.saving.saving

export const { setSaving } = savingSlice.actions
export default savingSlice.reducer
