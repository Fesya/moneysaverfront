import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectSaving, setSaving } from './SavingSlice'

export default function Saving () {
  const dispatch = useDispatch()
  const saving = useSelector(selectSaving)

  const handleChange = amount => {
    if (isNaN(amount)) { return }
    dispatch(setSaving({ saving: amount }))
  }

  return (
      <div>
        Total Saving:
        <input value={saving} onChange={event => handleChange(event.currentTarget.value)}/>
      </div>
  )
}
