import React from 'react'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Income from '../../components/income/Income'
import Saving from '../../components/saving/Saving'
import Statistics from '../../components/Statistics'
import PlannedExpenses from '../../components/expenses/PlannedExpenses'

export default class MoneySaver extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      newExpense: '',
      savings: 5.00,
      daysAmount: 31
    }
  }

  render () {
    return (
            <Container>
                <Row>
                    <Col md="auto">
                        <Row>
                            <Income/>
                        </Row>
                        <Row>
                            <Saving/>
                        </Row>
                        <Row>
                            <Statistics/>
                        </Row>
                    </Col>
                    <Col md="auto">
                        <PlannedExpenses/>
                    </Col>
                    <Col md="auto">
                        DAILY expences
                    </Col>
                </Row>
            </Container>
    )
  }
}
