import React from 'react'
import './App.css'
import MoneySaver from '../containers/MoneySaver/MoneySaver'

function App () {
  return (
        <div className="App">
            <MoneySaver/>
        </div>
  )
}

export default App
