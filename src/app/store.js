import { configureStore } from '@reduxjs/toolkit'
import incomeReducer from '../components/income/IncomeSlice'
import savingReducer from '../components/saving/SavingSlice'
import expensesReducer from '../components/expenses/PlannedExpensesSlice'

export default configureStore({
  reducer: {
    income: incomeReducer,
    saving: savingReducer,
    plannedExpenses: expensesReducer
  }
})
